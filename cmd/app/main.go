package main

//#cgo CFLAGS: -Wall -I../../pkg/ncur
//#cgo LDFLAGS: -L../../libs -lncur -lncurses
//#include "ncur.h"
import "C"

import (
	"fmt"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

func clearString(str string) string {

	const placeholder = ""

	replacer := strings.NewReplacer(
		"\n", placeholder,
		"\r", placeholder,
		"\t", placeholder,
		"\r\n", placeholder,
	)

	return replacer.Replace(str)
}

func readCurrentChargeValue(fileName string) (int, error) {

	data := make([]byte, 3)

	file, err := os.Open(fileName)
	if err != nil {
		return -1, err
	}
	defer file.Close()

	_, err = file.Read(data)
	if err != nil {
		return -1, err
	}

	str := clearString(string(data))
	result, err := strconv.Atoi(str)
	if err != nil {
		return -1, err
	}

	return result, nil
}

func writeNewChargeValue(fileName string, newValue int) error {

	data := []byte(strconv.Itoa(newValue))

	file, err := os.Create(fileName)
	if err != nil {
		return err
	}
	defer file.Close()

	file.Write(data)

	return nil
}

func editUnit(fileName string, newValue int) error {

	readFile, err := os.ReadFile(fileName)
	if err != nil {
		return err
	}

	oldData := strings.Split(string(readFile), "\n")

	for i, line := range oldData {
		if strings.Contains(line, "/sys/class/power_supply/BAT0/charge_control_end_threshold") {
			oldData[i] = "ExecStart=/bin/bash -c 'echo " + strconv.Itoa(newValue) + " > /sys/class/power_supply/BAT0/charge_control_end_threshold'"
		}
	}

	newData := []byte(strings.Join(oldData, "\n"))

	writeFile, err := os.Create(fileName)
	if err != nil {
		return err
	}
	defer writeFile.Close()

	writeFile.Write(newData)

	return nil
}

func main() {

	currentValue, err := readCurrentChargeValue("/sys/class/power_supply/BAT0/charge_control_end_threshold")
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	newValue := int(C.SetChargVal(C.int(currentValue)))
	if newValue == -1 {
		fmt.Println("C-function completed with error.")
		return
	}

	if newValue > 0 {
		if err := writeNewChargeValue("/sys/class/power_supply/BAT0/charge_control_end_threshold", newValue); err != nil {
			fmt.Println(err.Error())
			return
		}

		if err := editUnit("/etc/systemd/system/battery-charge-threshold.service", newValue); err != nil {
			fmt.Println(err.Error())
			return
		}

		cmd := exec.Command("systemctl", "daemon-reload")
		if err := cmd.Run(); err != nil {
			fmt.Println(err.Error())
			return
		}
	}

	/*cmd := exec.Command("clear")
	cmd.Stdout = os.Stdout
	if err := cmd.Run(); err != nil {
		fmt.Println(err.Error())
	}*/
}
