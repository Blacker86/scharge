#include <ncurses.h>
#include "ncur.h"

#define KEY_RETURN 10
#define KEY_SPACE  32

int SetChargVal(const int val)
{
	short int current_val = val;

	char message[] = "Set the maximum battery charge";
	short int items[] = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100};
	short int non_stand_val = 0;
	short int current_item;
	short int final_arr_size = sizeof(items)/sizeof(items[0]);;

	const short int width = 15;
	const short int height = 14;
	const short int offsetx = (sizeof(message)/sizeof(message[0]) - width)/2;
	const short int offsety = 1;

	for (short int i=0; i < final_arr_size; i++)
	{
		if (current_val == items[i])
		{
			current_item = i;
			break;
		}
		else if((current_val > items[i]) && (current_val < items[i+1]))
		{
			current_item = i+1;
			final_arr_size++;
			non_stand_val = 1;
			break;
		}
	}

	short int final_arr[final_arr_size];

	if (!initscr())
	{
		return -1;
	}

	curs_set(0);

	WINDOW *win = newwin(height, width, offsety, offsetx);
	if(win==NULL) return -1;

	short int curr_flag = 0;
	for (short int i=0; i<final_arr_size; i++)
	{

		if (i == current_item)
		{
			wattron(win, A_STANDOUT);

			if(non_stand_val)
			{
				mvwprintw(win, i+1, 4, "X %3d%%", current_val);
				final_arr[i] = current_val;
				curr_flag = 1;	
			}
			else
			{
				mvwprintw(win, i+1, 4, "X %3d%%", items[i]);
				final_arr[i] = items[i];
			}

			wattroff(win, A_STANDOUT);
		}
		else
		{
			mvwprintw(win, i+1, 4, "%5d%%", items[i-curr_flag]);
			final_arr[i] = items[i-curr_flag];
		}
	}

	wresize(win, final_arr_size+4, width);
	mvaddstr(0,1,message);
	box(win, 0, 0);
	mvwprintw(win, final_arr_size+2, 2, "%-s", "Save");
	mvwprintw(win, final_arr_size+2, 9, "%s", "Exit");
	refresh();

	noecho();
	keypad(win, TRUE);

	short int ch;

	short int i = current_item;
	while ((ch = wgetch(win)) != EOF)
	{
		
		short int previous_item = -1;
		if ((i < final_arr_size) && (i == current_item)) mvwprintw(win, i+1, 4, "X %3d%%", final_arr[i]);
		else if (i < final_arr_size) mvwprintw(win, i+1, 4, "%5d%%", final_arr[i]);

		mvwprintw(win, final_arr_size+2, 2, "%-s", "Save");
		mvwprintw(win, final_arr_size+2, 9, "%s", "Exit");
		switch(ch){
			case KEY_SPACE:
				if (i < final_arr_size)
				{
					previous_item = current_item;
					current_item = i;
					current_val	= final_arr[i];
				}
				break;
			case KEY_RETURN:
				if (i < final_arr_size)
				{
					previous_item = current_item;
					current_item = i;
					current_val	= final_arr[i];
				}
				else if (i == final_arr_size+2)
				{
					delwin(win);
					endwin();
					return current_val;
				}
				else if (i == final_arr_size+3)
				{
					delwin(win);
					endwin();
					return 0;
				}
				break;
			case KEY_UP:
				i--;
				if(i < 0) i = final_arr_size+3;
				else if ((i>=final_arr_size)&&(i<final_arr_size+2)) i = final_arr_size-1;
				break;
			case KEY_DOWN:
				i++;
				if (i > final_arr_size+3) i = 0;
				else if ((i >=final_arr_size) && (i < final_arr_size+2)) i = final_arr_size+2;
				break;

		}

		if ((i < final_arr_size) && (i == current_item))
		{
			wattron(win, A_STANDOUT);
			mvwprintw(win, i+1, 4, "X %3d%%", final_arr[i]);
			wattroff(win, A_STANDOUT);
			if ((previous_item > -1) && (previous_item != i)) mvwprintw(win, previous_item+1, 4, "%5d%%", final_arr[previous_item]);
		}
		else if (i < final_arr_size)
		{
			wattron(win, A_STANDOUT);
			mvwprintw(win, i+1, 4, "%5d%%", final_arr[i]);
			wattroff(win, A_STANDOUT);
		}
		else if (i == final_arr_size+3)
		{
			wattron(win, A_STANDOUT);
			mvwprintw(win, final_arr_size+2, 9, "%s", "Exit");
			wattroff(win, A_STANDOUT);
		}
		else if (i == final_arr_size+2)
		{
			wattron(win, A_STANDOUT);
			mvwprintw(win, final_arr_size+2, 2, "%-s", "Save");
			wattroff(win, A_STANDOUT);
		}
	}
	
	delwin(win);
	endwin();

	return current_val;
}