scharge: libncur.a
	go build -a -o $(CURDIR)/bin/scharge $(CURDIR)/cmd/app/main.go

libncur.a: clean
	gcc -Wall -Wparentheses -o $(CURDIR)/libs/ncur.o -c $(CURDIR)/pkg/ncur/ncur.c
	ar crs $(CURDIR)/libs/libncur.a $(CURDIR)/libs/ncur.o
	rm $(CURDIR)/libs/ncur.o

clean:
	rm -rf $(CURDIR)/bin $(CURDIR)/libs
	mkdir $(CURDIR)/libs

