# SCharge
The application allows you to set the battery charge limit of the laptop.

Works only on Linux.

Written in the go language using cgo technology and the ncurses library.

## Build
```
make
```

## Settings
Create a file /sys/class/power_supply/BAT0/charge_control_end_threshold. Write the required value to it.

Create a unit /etc/systemd/system/battery-charge-threshold.service
```
[Unit]
Description=Set the battery charge threshold
After=multi-user.target
StartLimitBurst=0

[Service]
Type=oneshot
Restart=on-failure
ExecStart=/bin/bash -c 'echo 60 > /sys/class/power_supply/BAT0/charge_control_end_threshold'

[Install]
WantedBy=multi-user.target
```
Start the service:
```
sudo systemctl daemon-reload
sudo systemctl start battery-charge-threshold.service
sudo systemctl enable battery-charge-threshold.service
```
Now you can set the battery charge limit in the terminal

![screen](./screen.png)